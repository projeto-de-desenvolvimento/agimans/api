<div align="center"><img width="250" src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/17412335/Agimans-logo-simples.png"></div>

# Agimans - Agile Management Software
[Agimans](https://gitlab.com/projeto-de-desenvolvimento/agimans/api/-/wikis/home) é um sistema de gestão de projetos, que se baseia em metodologias ágeis, principalmente Scrum


## Instalação
### Configuração do .env
Criar arquivo .env na raiz do projeto

Mais detalhes na página de [Configuração do .env](https://gitlab.com/projeto-de-desenvolvimento/agimans/api/-/wikis/Configura%C3%A7%C3%A3o-do-.env) na Wiki


### Banco de Dados
Será necessária a ferramenta **Docker** para criar do banco de dados

```bash
docker-compose up -d
```


### Instalação das Dependências
Recomendado utilizar **Node v12.17.0** para melhor reprodução do sistema

```bash
yarn
```


### Rodar o Projeto
#### Desenvolvimento
```bash
yarn start
```

#### Desenvolvimento Modo Watch
```bash
yarn start:dev
```

#### Produção
```bash
yarn start:prod
```


## Contato
- E-Mail: [brunoferrazsilveira@outlook.com.br](brunoferrazsilveira@outlook.com.br)
- GitLab: [brunofsilveira](https://gitlab.com/brunofsilveira)