import { ConfigModule } from '@nestjs/config'
import { GraphQLModule } from '@nestjs/graphql'
import { Module } from '@nestjs/common'
import { PassportModule } from '@nestjs/passport'
import { TypeOrmModule } from '@nestjs/typeorm'

import { AuthModule } from './modules/auth/auth.module'
import { GroupModule } from './modules/group/group.module'
import { IssueModule } from './modules/issue/issue.module'
import { ListModule } from './modules/list/list.module'
import { SprintModule } from './modules/sprint/sprint.module'
import { StatusModule } from './modules/status/status.module'
import { UserModule } from './modules/user/user.module'
@Module({
  controllers: [],
  imports: [
    AuthModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: true,
    }),
    GroupModule,
    IssueModule,
    ListModule,
    PassportModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: process.env.POSTGRES_DB,
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      port: Number(process.env.PORT),
      autoLoadEntities: true,
      synchronize: true,
      logging: true,
    }),
    SprintModule,
    StatusModule,
    UserModule,
  ],
  providers: [],
})
export class AppModule {}
