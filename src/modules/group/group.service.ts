import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm'

import { Repository } from 'typeorm'

import { Group } from './group.entity'

@Injectable()
export class GroupService extends TypeOrmQueryService<Group> {
  constructor(@InjectRepository(Group) groupRepository: Repository<Group>) {
    super(groupRepository, { useSoftDelete: true })
  }
}
