import { HideField } from '@nestjs/graphql'

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

import { Issue } from '../issue/issue.entity'
import { Sprint } from '../sprint/sprint.entity'
import { Status } from '../status/status.entity'
import { User } from '../user/user.entity'

@Entity()
export class Group {
  @HideField()
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ default: true })
  active: boolean

  @JoinTable()
  @ManyToMany(
    type => Group,
    group => group.childGroups,
  )
  childGroups?: Group[]

  @JoinColumn()
  @ManyToOne(
    type => Group,
    group => group.id,
  )
  fatherGroup?: Group

  @HideField()
  @OneToMany(
    type => Issue,
    issue => issue.group,
  )
  issues?: Issue[]

  @HideField()
  @OneToMany(
    type => Sprint,
    sprint => sprint.group,
  )
  sprints?: Sprint[]

  @HideField()
  @OneToMany(
    type => Status,
    status => status.project,
  )
  statuses?: Status[]

  @HideField()
  @JoinTable()
  @ManyToMany(
    type => User,
    user => user.groups,
  )
  users?: User[]

  @Column({ unique: true })
  name!: string

  @Column()
  project!: boolean

  @Column({ unique: true })
  slug!: string | null

  @Column()
  description?: string | null

  @CreateDateColumn()
  createdAt?: Date | null

  @UpdateDateColumn({ nullable: true })
  updatedAt?: Date | null

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date | null
}
