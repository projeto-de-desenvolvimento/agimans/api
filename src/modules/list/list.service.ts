import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm'

import { Repository } from 'typeorm'

import { List } from './list.entity'

@Injectable()
export class ListService extends TypeOrmQueryService<List> {
  constructor(@InjectRepository(List) listRepository: Repository<List>) {
    super(listRepository, { useSoftDelete: true })
  }
}
