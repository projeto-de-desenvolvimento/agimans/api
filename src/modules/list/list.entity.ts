import { HideField } from '@nestjs/graphql'

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

import { Issue } from '../issue/issue.entity'
import { Sprint } from '../sprint/sprint.entity'
import { Status } from '../status/status.entity'

@Entity()
export class List {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ default: true })
  active: boolean

  @Column()
  @Generated('increment')
  index!: number

  @HideField()
  @OneToMany(
    type => Issue,
    issue => issue.list,
  )
  issues?: Issue[]

  @HideField()
  @JoinColumn()
  @ManyToOne(
    type => Sprint,
    sprint => sprint.lists,
  )
  sprint?: Sprint

  @HideField()
  @JoinColumn()
  @ManyToOne(
    type => Status,
    status => status.lists,
  )
  status?: Status

  @Column()
  name: string

  @Column()
  description?: string

  @CreateDateColumn()
  createdAt?: Date | null

  @UpdateDateColumn({ nullable: true })
  updatedAt?: Date | null

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date | null
}
