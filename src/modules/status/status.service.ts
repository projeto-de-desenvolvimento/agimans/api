import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm'

import { Repository } from 'typeorm'

import { Status } from './status.entity'

@Injectable()
export class StatusService extends TypeOrmQueryService<Status> {
  constructor(@InjectRepository(Status) statusRepository: Repository<Status>) {
    super(statusRepository, { useSoftDelete: true })
  }
}
