import { Module } from '@nestjs/common'
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql'
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm'

import { Status } from './status.entity'
import { StatusDTO } from './status.dto'
import { StatusService } from './status.service'

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Status])],
      resolvers: [{ DTOClass: StatusDTO, EntityClass: Status }],
    }),
  ],
  providers: [StatusService],
})
export class StatusModule {}
