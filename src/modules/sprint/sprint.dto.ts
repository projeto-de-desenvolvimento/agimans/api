import { Connection, FilterableField, Relation } from '@nestjs-query/query-graphql'
import { Field, GraphQLISODateTime, HideField, Int, ObjectType } from '@nestjs/graphql'

import { Group } from '../group/group.entity'
import { GroupDTO } from '../group/group.dto'
import { Issue } from '../issue/issue.entity'
import { IssueDTO } from '../issue/issue.dto'
import { List } from '../list/list.entity'
import { ListDTO } from '../list/list.dto'

@Connection('issue', () => IssueDTO, { nullable: true })
@Connection('list', () => ListDTO, { nullable: true })
@ObjectType('Sprint')
@Relation('group', () => GroupDTO, { nullable: true })
export class SprintDTO {
  @FilterableField(() => Int)
  id!: number

  @FilterableField()
  active!: boolean

  @FilterableField()
  index?: number

  @HideField()
  group?: Group

  @HideField()
  issues?: Issue[]

  @HideField()
  lists?: List[]

  @FilterableField()
  endDate: Date

  @FilterableField()
  startDate: Date

  @FilterableField()
  title: string

  @Field()
  description?: string

  @Field({ nullable: true })
  estimate?: number

  @Field({ nullable: true })
  spend?: number

  @Field({ nullable: true })
  weight?: number

  @Field(() => GraphQLISODateTime)
  createdAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  updatedAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  deletedAt?: Date | null
}
