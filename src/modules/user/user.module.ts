import { Module } from '@nestjs/common'
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql'
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm'

import { User } from './user.entity'
import { UserDTO } from './user.dto'
import { UserService } from './user.service'

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([User])],
      resolvers: [{ DTOClass: UserDTO, EntityClass: User }],
    }),
  ],
  providers: [UserService],
})
export class UserModule {}
