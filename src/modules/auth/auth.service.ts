import { Injectable } from '@nestjs/common'
import { InjectEntityManager } from '@nestjs/typeorm'
import { JwtService } from '@nestjs/jwt'

import { EntityManager } from 'typeorm'

import { AuthDTO } from './auth.dto'
import { User } from '../user/user.entity'

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}

  findById(id: number): Promise<User> {
    return this.entityManager.findOne(User, id)
  }

  async login({ email, password }): Promise<AuthDTO> {
    const user = await this.entityManager.findOne(User, {
      where: [{ email: email }],
    })

    if (!user) throw new Error('Usuário não cadastrado no sistema')
    if (!user.passwordMatches(password)) throw new Error('Senha inválida')

    return {
      token: this.jwtService.sign({ email: user.email }),
      email: user.email,
      name: user.name,
      password: user.password,
    }
  }
}
