import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType('Auth')
export class AuthDTO {
  @Field()
  token: string

  @Field()
  email?: string

  @Field()
  name?: string

  @Field()
  password?: string
}
