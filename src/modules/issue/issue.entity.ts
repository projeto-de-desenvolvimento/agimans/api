import { HideField } from '@nestjs/graphql'

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Generated,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

import { Group } from '../group/group.entity'
import { List } from '../list/list.entity'
import { Sprint } from '../sprint/sprint.entity'
import { Status } from '../status/status.entity'
import { User } from '../user/user.entity'

@Entity()
export class Issue {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ default: true })
  active: boolean

  @Column()
  @Generated('increment')
  index!: number

  @HideField()
  @ManyToOne(
    type => Group,
    group => group.issues,
  )
  group?: Group

  @HideField()
  @JoinColumn()
  @ManyToOne(
    type => List,
    list => list.issues,
  )
  list?: List

  @HideField()
  @JoinColumn()
  @ManyToOne(
    type => Sprint,
    sprint => sprint.issues,
  )
  sprint?: Sprint

  @HideField()
  @JoinTable()
  @ManyToMany(
    type => User,
    user => user.issues,
  )
  users?: User[]

  @HideField()
  @JoinColumn()
  @ManyToOne(
    type => Status,
    status => status.issues,
  )
  status?: Status

  @Column()
  title: string

  @Column()
  description?: string

  @Column({ nullable: true })
  estimate?: number

  @Column({ nullable: true })
  spend?: number

  @Column({ nullable: true })
  weight?: number

  @CreateDateColumn()
  createdAt?: Date | null

  @UpdateDateColumn({ nullable: true })
  updatedAt?: Date | null

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date | null
}
